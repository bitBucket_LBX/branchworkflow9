﻿using System;

namespace numberCounter
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int input;
            int sum = 0;
            Console.WriteLine("Please enter a number ");
            input = Convert.ToInt32(Console.ReadLine());


             for ( int i = 1; i <= input ; i++)
            {
                Console.Write(i + " ");
                sum += i;
            }

            Console.Write("The sum is :"  + sum);

        }
    }
}
